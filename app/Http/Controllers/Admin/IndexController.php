<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Post;

class IndexController extends Controller
{
    public function __invoke()
    {
        $postsFromDb = Post::all();
        $posts = array();
        foreach ($postsFromDb as $dbPost) {
            array_push($posts, [
                'id' => $dbPost->id,
                'title' => $dbPost->title,
                'category' => $dbPost->category,
                'tags' => $dbPost->tags,
                'published_at' => $dbPost->timestamps,
                'content' => $dbPost->content,
            ]);
        }
        return view('admin.index', compact('posts'));
    }


}
