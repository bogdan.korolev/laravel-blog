<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Tag;
use Illuminate\Http\Request;

class TagController extends Controller
{
    public function index()
    {
        dd($this->_getTitles(Tag::all()));
    }

    public function create(Request $request) {
        $data = $request->validate([
            'title' => 'required|string',
        ]);
        Tag::firstOrCreate([
            'title' => $data['title']
        ]);
        return redirect()->route('admin');
    }

    public function show(Tag $tag)
    {
        $posts = $tag->posts;
        return view('admin.tag', compact('tag', 'posts'));
    }

    public function edit(Request $request, Tag $tag) {
        $tagToUpd = Tag::find($tag->id);
        $tagToUpd->update([
            'title' => $request->title,
        ]);
        return redirect()->route('admin');
    }

    public function delete(Tag $tag) {
        $categoryToUpd = Tag::find($tag);
        $categoryToUpd->delete();
        return redirect()->route('admin');
    }

    private function _getTitles($dbRequestCollection) {
        return $dbRequestCollection->map(function ($el) {return 'id: ' . $el->id . '; title: ' . $el->title;});
    }
}
