<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\View\View;

class PostController extends Controller
{
    public function index()
    {
        //
    }

    public function create(Request $request)
    {
        return view('admin.posts.add');
    }

    public function store(Request $request)
    {
        dump($request);
    }

    public function show(Post $post): View
    {
        $category = $post->category;
        $tags = $post->tags;
        return view('admin.posts.show', compact('post', 'category', 'tags'));
    }

    public function edit(Request $request, $id): View
    {
        return view('admin.posts.edit');
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

    private function _getTitles($dbRequestCollection) {
        return $dbRequestCollection->map(function ($el) {return 'id: ' . $el->id . '; title: ' . $el->title;});
    }
}
