<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Post;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        dd($this->_getTitles(Category::all()));
    }

    public function create(Request $request) {
        $data = $request->validate([
            'title' => 'required|string',
        ]);
        Category::firstOrCreate([
            'title' => $data['title']
        ]);
        return redirect()->route('admin');
    }

    public function show(Category $category)
    {
        $posts = $category->posts;
        return view('admin.category', compact('category', 'posts'));
    }

    public function edit(Request $request, Category $category) {
        $categoryToUpd = Category::find($category->id);
        $categoryToUpd->update([
            'title' => $request->title,
        ]);
        return redirect()->route('admin');
    }

    public function delete($categoryId) {
        $categoryToUpd = Category::find($categoryId);
        $categoryToUpd->delete();
        return redirect()->route('admin');
    }

    private function _getTitles($dbRequestCollection) {
        return $dbRequestCollection->map(function ($el) {return 'id: ' . $el->id . '; title: ' . $el->title;});
    }
}
