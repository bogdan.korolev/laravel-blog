@extends('admin.layouts.main')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="mb-2">
                        {{ $post->title }}
                    </h1>
                    <div class="d-flex flex-wrap">
                        @foreach($tags as $tag)
                            <a href="#" class="badge badge-warning mr-1">
                                #{{ $tag->title }}
                            </a>
                        @endforeach
                    </div>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="{{ route('admin.categories') }}">
                                All
                            </a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="{{ route('admin.categories.show', $category) }}">
                                {{ $category->title }}
                            </a>
                        </li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <p class="m-0">
                        {{ $post->content }}
                    </p>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
