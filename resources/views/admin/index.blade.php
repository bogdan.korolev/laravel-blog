@extends('admin.layouts.main')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Dashboard</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        {{--                        <li class="breadcrumb-item active">Dashboard v1</li>--}}
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-sm-6 col-md-5 col-lg-4 col-xl-3">
                    @include('admin.includes.categories-card')
                </div>
                <!-- /.col -->
                <div class="col-sm-6 col-md-5 col-lg-4 col-xl-3">
                    @include('admin.includes.tags-card')
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header d-flex align-items-center">
                            <h3 class="card-title">Posts</h3>
                            <a href="{{ route('admin.posts.create') }}" class="ml-auto btn btn-outline-success">
                                <i class="fas fa-plus-square"></i>
                            </a>
                        </div>
                        <!-- ./card-header -->
                        <div class="card-body">
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>id</th>
                                    <th>title</th>
                                    <th>category</th>
                                    <th>tags</th>
                                    <th>action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($posts as $post)
                                    <tr data-widget="expandable-table" aria-expanded="false">
                                        <td class="align-middle">
                                            {{ $post['id'] }}
                                        </td>
                                        <td class="align-middle">
                                            <p class="m-0 text-truncate">
                                                {{ $post['title'] }}
                                            </p>
                                        </td>
                                        <td class="align-middle">
                                            <p class="m-0 text-truncate">
                                                {{ $post['category']->title }}
                                            </p>
                                        </td>
                                        <td class="align-middle">
                                            @foreach($post['tags'] as $tag)
                                                <span href="#" class="badge badge-warning m-1">
                                                    #{{ $tag->title }}
                                                </span>
                                            @endforeach
                                        </td>
                                        <td class="align-middle">
                                            <div class="btn-group">
                                                <a href="{{ route('admin.posts.show', $post['id']) }}" class="btn btn-outline-primary">
                                                    <i class="fas fa-external-link-square-alt"></i>
                                                </a>
                                                <a href="{{ route('admin.posts.edit', $post['id']) }}" class="btn btn-outline-success">
                                                    <i class="fas fa-pen-alt"></i>
                                                </a>
                                                <a href="{{ route('admin.posts.delete', $post['id']) }}" class="btn btn-outline-danger">
                                                    <i class="fas fa-trash-alt"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="expandable-body">
                                        <td colspan="5">
                                            <p>
                                                {{ $post['content'] }}
                                            </p>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
