<?php
    $tags = \App\Tag::all();
?>

<div class="card card-primary card-outline card-outline-tabs card-">
    <div class="card-header p-0 border-bottom-0">
        <ul class="nav nav-tabs" id="tags-card" role="tablist">
            <li class="px-3 d-flex align-items-center">
                <h3 class="card-title">
                    <i class="fas fa-hashtag"></i>
                </h3>
            </li>
            <li class="nav-item">
                <a class="nav-link active" id="tags-card-list" data-toggle="pill"
                   href="#tags-card-tabs-four-list" role="tab" aria-controls="tags-card-tabs-four-list" aria-selected="true">
                    List
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="tags-card-add-tab" data-toggle="pill"
                   href="#tags-card-tabs-four-add" role="tab" aria-controls="tags-card-tabs-four-add"
                   aria-selected="false">
                    Add
                </a>
            </li>
            <li class="card-tools ml-auto">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                </button>
            </li>
        </ul>
    </div>
    <div class="card-body p-0">
        <div class="tab-content" id="tags-cardContent">
            <div class="tab-pane fade active show" id="tags-card-tabs-four-list" role="tabpanel"
                 aria-labelledby="tags-card-list">
                @if(count($tags))
                    <div id="accordion">
                        @foreach($tags as $tag)
                            <div class="card card-primary card-outline mb-0">
                                <div class="w-100">
                                    <div class="card-header d-flex align-items-center justify-content-between">
                                        <h4 class="card-title w-100 mb-0">
                                            <i class="fas fa-hashtag mr-2"></i>
                                            <a href="{{ route('admin.categories.show', $tag) }}"
                                               class="font-weight-bold text-monospace">
                                                {{$tag->title}}
                                            </a>
                                        </h4>
                                        <div class="d-flex ">
                                            <a href="#collapse-tag-{{ $tag->id }}" class="text-success text-lg mr-2"
                                               data-toggle="collapse" aria-expanded="false">
                                                <i class="fas fa-pen"></i>
                                            </a>
                                            <a href="{{ route('admin.categories.delete', [$tag->id]) }}"
                                               class="text-danger text-lg ml-2">
                                                <i class="fas fa-trash-alt"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div id="collapse-tag-{{ $tag->id }}" class="collapse" data-parent="#accordion"
                                     style="">
                                    <div class="card-body">
                                        <form action="{{ route('admin.tags.edit', $tag) }}" method="POST">
                                            @csrf
                                            <div class="form-group row mb-0">
                                                <label for="categoryTitle" class="col-3 col-form-label">Title</label>
                                                <div class="col">
                                                    <input
                                                        type="text"
                                                        class="form-control"
                                                        id="categoryTitle"
                                                        placeholder="Category title"
                                                        name="title"
                                                        value="{{ $tag->title }}"
                                                    >
                                                </div>
                                            </div>
                                            @error('title')
                                            <div class="text-danger small">
                                                {{$message}}
                                            </div>
                                            @enderror
                                            <button type="submit" class="btn btn-primary ml-auto mt-2">
                                                Submit
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @else
                    <div class="px-3 py-2">
                        <p class="m-0">
                            List is empty
                        </p>
                    </div>
                @endif
            </div>
            <div class="tab-pane fade" id="tags-card-tabs-four-add" role="tabpanel"
                 aria-labelledby="tags-card-add-tab"
            >
                <form action="{{ route('admin.tags.create') }}" method="POST">
                    @csrf
                    <div class="px-3 pt-2">
                        <div class="form-group row mb-0">
                            <label for="categoryTitle" class="col-3 col-form-label">Title</label>
                            <div class="col">
                                <input
                                    type="text"
                                    class="form-control"
                                    id="categoryTitle"
                                    placeholder="Category title"
                                    name="title"
                                >
                            </div>
                        </div>
                        @error('title')
                            <div class="text-danger small">
                                {{$message}}
                            </div>
                        @enderror
                        <button type="submit" class="btn btn-primary ml-auto mt-2">
                            Submit
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.card -->
</div>
