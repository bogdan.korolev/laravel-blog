@extends('admin.layouts.main')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-monospace">
                        <i class="fas fa-hashtag"></i>
                        {{ $tag->title }}
                    </h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="{{ route('admin') }}">
                                Home
                            </a>
                        </li>
                        <li class="breadcrumb-item active">Tag</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="card card-default">
                        <div class="card-header">
                            <h3 class="card-title">
                                <i class="fas fa-bullhorn"></i>
                                Posts
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            @foreach($posts as $post)
                                <div class="callout callout-info">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <h5>
                                            {{ $post->title }}
                                        </h5>
                                        <a href="{{ route('admin.posts.show', $post) }}" class="btn btn-outline-success">
                                            <i class="fas fa-external-link-square-alt"></i>
                                        </a>
                                    </div>
                                    <p class="text-truncate">
                                        {{ $post->content }}
                                    </p>
                                </div>
                            @endforeach
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
