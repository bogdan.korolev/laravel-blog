<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function () {
   Route::get('/', 'IndexController')->name('admin');

    Route::group(['prefix' => 'categories'], function () {
        Route::get('/', 'CategoryController@index')->name('admin.categories');
        Route::post('/create', 'CategoryController@create')->name('admin.categories.create');
        Route::get('/{category}', 'CategoryController@show')->name('admin.categories.show');
        Route::post('/{category}/edit', 'CategoryController@edit')->name('admin.categories.edit');
        Route::get('/{category}/delete', 'CategoryController@delete')->name('admin.categories.delete');
    });

    Route::group(['prefix' => 'tags'], function () {
        Route::get('/', 'TagController@index')->name('admin.tags');
        Route::post('/create', 'TagController@create')->name('admin.tags.create');
        Route::get('/{tag}', 'TagController@show')->name('admin.tags.show');
        Route::get('/{tag}/edit', 'TagController@edit')->name('admin.tags.edit');
        Route::get('/{tag}/delete', 'TagController@delete')->name('admin.tags.delete');
    });

    Route::group(['prefix' => 'posts'], function () {
        Route::get('/', 'PostController@index')->name('admin.posts');

        Route::get('/create', 'PostController@create')->name('admin.posts.create');
        Route::post('/', 'PostController@store')->name('admin.posts.store');

        Route::get('/{post}', 'PostController@show')->name('admin.posts.show');

        Route::get('/{post}/edit', 'PostController@edit')->name('admin.posts.edit');
        Route::post('/{post}', 'PostController@update')->name('admin.posts.update');

        Route::get('/{post}/delete', 'PostController@destroy')->name('admin.posts.delete');
    });
});
